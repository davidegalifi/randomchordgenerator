﻿using System.ComponentModel.DataAnnotations;

namespace Jamkie.Web.Models
{
    public class Feedback
    {
        [Required]
        public string email { get; set; }

        public int score{ get; set; }

        [Required]
        public string comment { get; set; }
    }
}