﻿var settings = require('./../../shared/settings');

module.exports = {
    init: function () {
        var app = angular.module('register', ['ngCookies']);

        app.controller('RegisterController', ['$rootScope', '$scope', '$location', '$http', '$cookies', function ($rootScope, $scope, $location, $http, $cookies) {
            $cookies.remove('usr_token');
            renderPage();

            $scope.submit = function () {
                $scope.isLoading = true;
                $scope.showError = false;

                $http({
                    method: 'POST',
                    url: settings.webApiBaseUrl + 'api/accounts/register',
                    data: {
                        "Email": $scope.form.email,
                        "Password": $scope.form.password,
                        "ConfirmPassword": $scope.form.password
                    }
                }).error(function (data, status) {
                    if (status === 400 && inArray("is already taken", data.modelState["error"]))
                        $scope.error = 'it looks like you have an account already';
                    else
                        $scope.error = 'error during the registration. please try again in a couple of minutes. it that still persists contact us.';

                    $scope.isLoading = false;
                    $scope.showError = true;
                })
                    .success(function (data) {
                        $scope.isLoading = false;
                        $scope.hideForm = true;
                    });
            }

            /****** RENDER PAGE *****/
            function renderPage() {
                $scope.isLoading = false;
                $scope.hideForm = false;
                $scope.showError = false;
                $scope.error = '';

                // set scope Location
                if ($location.path() === '/home' || $location.path() === '/' || $location.path() === '')
                    $rootScope.location = 'home';
                else
                    $rootScope.location = $location.path();
            }

            function inArray(word, array) {
                var found = false;
                for (var i = 0; i < array.length && !found; i++) {
                    if (array[i].indexOf(word) >= 0) {
                        found = true;
                        break;
                    }
                }
                return found;
            }
        }]);
    }
}