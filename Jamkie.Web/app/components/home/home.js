﻿var home = {
    init: function () {
        var app = angular.module('home', ['serviceModule']);

        app.controller('HomeController', ['$scope', '$rootScope','authenticationService', function ($scope, $rootScope, authenticationService) {
            $scope.setLocation = function (path) {
                $rootScope.location = path;
            };

            $scope.loggedIn = authenticationService.isUserLogged();
        }]);
    }
};

module.exports = {
    init: home.init
}