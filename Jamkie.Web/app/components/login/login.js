﻿var settings = require('./../../shared/settings');

module.exports = {
    init: function () {
        var app = angular.module('login', ['ngCookies', 'serviceModule']);

        app.controller('LoginController', ['$rootScope', '$scope', '$location', '$http', '$cookies', 'authenticationService', function ($rootScope, $scope, $location, $http, $cookies, authenticationService) {

            $cookies.remove('usr_token');
            renderPage();

            $scope.submit = function () {
                $scope.isLoading = true;
                $scope.showError = false;

                $http({
                    method: 'POST',
                    url: settings.webApiBaseUrl + 'login',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: {
                        "grant_type": "password",
                        "username": $scope.form.email,
                        "password": $scope.form.password
                    }
                })
                    .error(function (data, status) {
                        $scope.showError = true;
                        if (status === 400) {
                            $scope.error = 'invalid credentials';
                        }
                        else {
                            $scope.error = 'generic error';
                        }

                        $scope.isLoading = false;
                })
                    .success(function (data) {
                        // call authService
                        authenticationService.login(data.access_token);
                        $location.path('/chord-progression')

                        $scope.isLoading = false;
                    });
            }


            /****** RENDER PAGE *****/
            function renderPage() {
                $scope.isLoading = false;
                $scope.hideForm = false;
                $scope.showError = false;
                $scope.error = '';

                // set scope Location
                if ($location.path() == '/home' || $location.path() == '/' || $location.path() == '')
                    $rootScope.location = 'home';
                else
                    $rootScope.location = $location.path();
            }
        }]);
    }
};