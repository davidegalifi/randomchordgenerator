﻿/// <reference path="/scripts/angular.js" />
var routes = {
    init: function () {
        var routeModule = angular.module('routeModule', ['ngRoute']);

        routeModule.config([
            '$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
                $routeProvider.
                    when('/', {
                        templateUrl: 'app/components/home/home.html',
                        controller: 'HomeController'
                    }).
                    when('/static-chord', {
                        templateUrl: 'app/components/static-chord/static-chord.html',
                        controller: 'StaticChordController'
                    }).
                    when('/feedback', {
                        templateUrl: 'app/components/feedback/feedback.html',
                        controller: 'FeedbackController'
                    }).
                    when('/chord-progression', {
                        templateUrl: 'app/components/chord-progression/chord-progression.html',
                        controller: 'ChordProgressionController'
                    }).
                    when('/register', {
                        templateUrl: 'app/components/register/register.html',
                        controller: 'RegisterController'
                    }).
                    when('/login',
                        {
                            templateUrl: 'app/components/login/login.html',
                            controller: 'LoginController'
                        }).
                    otherwise({
                        redirectTo: '/'
                    });

                // use the HTML5 History API
                $locationProvider.html5Mode(true);
            }
        ]);
    }
};

module.exports = {
    init: routes.init
}