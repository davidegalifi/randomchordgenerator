﻿using System.Data.Entity;
using Jamkie.Repositories;
using Jamkie.Repositories.Context;
using Jamkie.Repositories.Contracts;

namespace Jamkie.Service
{
    public class UnitOfWork : IUnitOfWork
    {
        public ITrackRepository Tracks { get; set; }

        private readonly DbContext _context;

        public UnitOfWork(DbContext context, ITrackRepository trackRepository)
        {
            _context = context;

            Tracks = trackRepository;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
