﻿using System;
using Jamkie.Repositories.Contracts;

namespace Jamkie.Service
{
    public interface IUnitOfWork : IDisposable
    {
        ITrackRepository Tracks { get; set; }

        int SaveChanges();
    }
}
