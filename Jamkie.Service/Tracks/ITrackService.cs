﻿using System.Linq;
using Jamkie.Domain;

namespace Jamkie.Service.Tracks
{
    public interface ITrackService
    {
        void AddTrack(Track track);

        IQueryable<Track> GetAllTracks(string userId);

        void UpdateTrack(Track track);

        void DeleteTrack(Track track);
    }
}
