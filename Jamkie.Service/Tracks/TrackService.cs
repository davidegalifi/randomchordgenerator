﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Jamkie.Domain;

namespace Jamkie.Service.Tracks
{
    public class TrackService : ITrackService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TrackService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddTrack(Track track)
        {
            if (_unitOfWork.Tracks.Find(x => x.UserId == track.UserId && x.Name.ToLower() == track.Name).Any())
                throw new DuplicateNameException("A track with this name already exists");

            _unitOfWork.Tracks.Add(track);
            _unitOfWork.SaveChanges();
        }

        public IQueryable<Track> GetAllTracks(string userId)
        {
            return _unitOfWork.Tracks.Find(x => x.UserId == userId);
        }

        public void UpdateTrack(Track track)
        {
            if (!_unitOfWork.Tracks.Find(x => x.UserId == track.UserId && x.Id == track.Id).Any())
                throw new KeyNotFoundException($"Can't find the track {track.Name}");

            _unitOfWork.Tracks.Update(track);

            _unitOfWork.SaveChanges();
        }

        public void DeleteTrack(Track track)
        {
            if (!_unitOfWork.Tracks.Find(x => x.UserId == track.UserId && x.Id == track.Id).Any())
                throw new KeyNotFoundException($"Can't find the track {track.Name}");

            _unitOfWork.Tracks.Remove(track);

            _unitOfWork.SaveChanges();
        }
    }
}
