﻿using SimpleInjector;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using Jamkie.Repositories;
using Jamkie.Repositories.Context;
using Jamkie.Repositories.Contracts;
using Jamkie.Service;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

namespace Jamkie.WebApi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}
