﻿namespace Jamkie.WebApi.Models.Tracks
{
    public class AddTracksRequest
    {
        public string Name { get; set; }

        public string Chords { get; set; }

        public int BarsCount { get; set; }

        public int Tempo { get; set; }

        public int Repeat { get; set; }
    }
}