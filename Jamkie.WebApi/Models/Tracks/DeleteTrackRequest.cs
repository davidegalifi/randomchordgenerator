﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jamkie.WebApi.Models.Tracks
{
    public class DeleteTrackRequest
    {
        public int Id { get; set; }
    }
}