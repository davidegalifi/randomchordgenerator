﻿using System.Security.Claims;
using System.Threading.Tasks;
using Jamkie.Domain;
using Jamkie.Repositories.Context;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;

namespace Jamkie.WebApi.Infrastructure
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (var ctx = JamkieDbContext.Create())
            {
                using (var userManager = new SsoUserManager(new UserStore<SsoUser>(ctx)))
                {
                    IdentityUser user = await userManager.FindAsync(context.UserName, context.Password);

                    if (user == null)
                    {
                        context.SetError("invalid_grant", "The user name or password is incorrect.");
                        return;

                    }

                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim("id", user.Id));
                    identity.AddClaim(new Claim("role", "user"));

                    context.Validated(identity);
                }
            }
        }
    }
}