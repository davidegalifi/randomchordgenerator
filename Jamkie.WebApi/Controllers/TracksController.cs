﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using Jamkie.Service;
using System.Web.Http;
using AutoMapper;
using Jamkie.Domain;
using Jamkie.Service.Tracks;
using Jamkie.WebApi.Models.Tracks;

namespace Jamkie.WebApi.Controllers
{
    [RoutePrefix("api/tracks")]
    public class TracksController : BaseApiController
    {
        private readonly ITrackService _trackService;
        private readonly IMapper _mapper;

        public TracksController(ITrackService trackService, IMapper mapper)
        {
            _trackService = trackService;
            _mapper = mapper;
        }

        // [Authorize]
        [Route("add")]
        [HttpPost]
        public IHttpActionResult AddTrack(AddTracksRequest request)
        {
            try
            {
                var track = _mapper.Map<AddTracksRequest, Track>(request);
                track.UserId = UserId;

                _trackService.AddTrack(track);

                return Ok();
            }
            catch (DuplicateNameException e)
            {
                var content = new HttpError(e.Message);
                return Content(HttpStatusCode.Conflict, content);
            }
        }

        // [Authorize]
        [Route("update")]
        [HttpPost]
        public IHttpActionResult UpdateTrack(UpdateTrackRequest request)
        {
            try
            {
                var track = _mapper.Map<UpdateTrackRequest, Track>(request);
                track.UserId = UserId;

                _trackService.UpdateTrack(track);

                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                var content = new HttpError(e.Message);
                return Content(HttpStatusCode.NotFound, content);
            }
        }

        // [Authorize]
        [Route("getAll")]
        [HttpGet]
        public IHttpActionResult GetAllTracks()
        {
            var tracks = _trackService.GetAllTracks(UserId);

            return Ok(tracks);
        }

        // [Authorize]
        [Route("delete")]
        [HttpPost]
        public IHttpActionResult Delete(DeleteTrackRequest request)
        {
            try
            {
                var track = _mapper.Map<DeleteTrackRequest, Track>(request);
                track.UserId = UserId;

                _trackService.DeleteTrack(track);

                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                var content = new HttpError(e.Message);
                return Content(HttpStatusCode.NotFound, content);
            }
        }
    }
}