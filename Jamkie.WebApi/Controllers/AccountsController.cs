﻿using Jamkie.WebApi.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Jamkie.Domain;

namespace Jamkie.WebApi.Controllers
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseApiController
    {
        [Route("users")]
        [Authorize]
        public IHttpActionResult GetUsers()
        {
            return Ok(SsoUserManager.Users.ToList().Select(u => TheModelFactory.Create(u)));
        }

        [Route("user/{id:guid}", Name = "GetUserById")]
        [Authorize]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            var user = await SsoUserManager.FindByIdAsync(id);

            if (user != null)
            {
                return Ok(TheModelFactory.Create(user));
            }

            return NotFound();
        }

        [Authorize]
        [Route("protected")]
        public IHttpActionResult Protected()
        {
            var user = User.Identity as ClaimsIdentity;

            if (user != null)
                return Ok(user.Claims.Single(x => x.Type == "id").Value);

            return NotFound();
        }

        [Route("user/{username}")]
        [Authorize]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var user = await SsoUserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(TheModelFactory.Create(user));
            }

            return NotFound();
        }

        [Route("register")]
        public async Task<IHttpActionResult> Register(RegisterRequest createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new SsoUser()
            {
                UserName = createUserModel.Email,
                Email = createUserModel.Email,
                Level = 3,
                JoinDate = DateTime.Now,
            };

            IdentityResult addUserResult = await SsoUserManager.CreateAsync(user, createUserModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Created(locationHeader, TheModelFactory.Create(user));
        }
    }
}