﻿using Jamkie.WebApi.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Jamkie.Repositories.Context;

namespace Jamkie.WebApi.Controllers
{
    public class BaseApiController : ApiController
    {
        private ModelFactory _modelFactory;
        private readonly SsoUserManager _appUserManager = null;

        protected SsoUserManager SsoUserManager => _appUserManager ?? Request.GetOwinContext().GetUserManager<SsoUserManager>();

        public string UserId
        {
            get
            {
                var user = User.Identity as ClaimsIdentity;
                return user?.Claims.Single(x => x.Type == "id").Value;
            }
        }

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(Request, SsoUserManager);
                }
                return _modelFactory;
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("error", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}