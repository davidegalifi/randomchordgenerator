﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;
using System.Linq;
using AutoMapper;
using Jamkie.Domain;
using Jamkie.Repositories;
using Jamkie.WebApi.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Jamkie.Repositories.Context;
using Jamkie.Repositories.Contracts;
using Jamkie.Service;
using Jamkie.Service.Tracks;
using Jamkie.WebApi.Models.Tracks;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

[assembly: OwinStartup(typeof(Jamkie.WebApi.Startup))]

namespace Jamkie.WebApi
{
    public class Startup
    {
        private IMapper _mapper;

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration httpConfig = new HttpConfiguration();

            ConfigureOAuthTokenGeneration(app);

            ConfigureWebApi(httpConfig);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(httpConfig);

            var container = new Container();

            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            // Mapping
            ConfigureMapping();

            // Register dependencies
            container.Register<DbContext, JamkieDbContext>(Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<ITrackService, TrackService>(Lifestyle.Scoped);
            container.Register<ITrackRepository, TrackRepository>(Lifestyle.Scoped);

            container.RegisterSingleton<IMapper>(_mapper);

            container.RegisterWebApiControllers(httpConfig);

            // Wrap response
            httpConfig.MessageHandlers.Add(new WrappingHandler());

            container.Verify();

            httpConfig.DependencyResolver =
                 new SimpleInjectorWebApiDependencyResolver(container);
        }

        private void ConfigureMapping()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AddTracksRequest, Track>();
                cfg.CreateMap<UpdateTrackRequest, Track>();
                cfg.CreateMap<DeleteTrackRequest, Track>();
            });

            _mapper = config.CreateMapper();
        }

        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {

            OAuthAuthorizationServerOptions oAuthAuthorizationServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(JamkieDbContext.Create);
            app.CreatePerOwinContext<SsoUserManager>(SsoUserManager.Create);

            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
