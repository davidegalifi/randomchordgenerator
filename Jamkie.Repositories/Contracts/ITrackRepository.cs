﻿using Jamkie.Domain;
using Jamkie.Repositories.Generic;

namespace Jamkie.Repositories.Contracts
{
    public interface ITrackRepository : IRepository<Track>
    {
        void Update(Track track);
    }
}
