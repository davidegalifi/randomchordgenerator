﻿using Jamkie.Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Jamkie.Repositories.Context
{
    public class SsoUserManager : UserManager<SsoUser>
    {
        public SsoUserManager(IUserStore<SsoUser> store) : base(store)
        {
        }

        public static SsoUserManager Create(IdentityFactoryOptions<SsoUserManager> options, IOwinContext context)
        {
            var dbContext = context.Get<JamkieDbContext>();
            var userManager = new SsoUserManager(new UserStore<SsoUser>(dbContext));

            return userManager;
        }
    }
}