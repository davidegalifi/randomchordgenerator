﻿using System.Data.Entity;
using Jamkie.Domain;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Jamkie.Repositories.Context
{
    public class JamkieDbContext : IdentityDbContext<SsoUser>
    {
        public JamkieDbContext() : base("Jamkie", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Track> Tracks { get; set; }
        
        public static JamkieDbContext Create()
        {
            return new JamkieDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Track>()
                .HasKey(s => s.Id)
                .HasRequired(s => s.SsoUser).WithMany(m => m.Tracks).HasForeignKey(f => f.UserId);
        }
    }
}