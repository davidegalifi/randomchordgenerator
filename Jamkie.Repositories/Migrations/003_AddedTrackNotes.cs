namespace Jamkie.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTrackNotes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tracks", "Notes", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tracks", "Notes");
        }
    }
}
