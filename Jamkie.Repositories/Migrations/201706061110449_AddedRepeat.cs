namespace Jamkie.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRepeat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tracks", "Repeat", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tracks", "Repeat");
        }
    }
}
