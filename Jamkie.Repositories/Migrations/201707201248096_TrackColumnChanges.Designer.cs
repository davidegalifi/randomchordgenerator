// <auto-generated />
namespace Jamkie.Repositories.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class TrackColumnChanges : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TrackColumnChanges));
        
        string IMigrationMetadata.Id
        {
            get { return "201707201248096_TrackColumnChanges"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
