namespace Jamkie.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrackColumnChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tracks", "Chords", c => c.String());
            DropColumn("dbo.Tracks", "Notes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tracks", "Notes", c => c.String());
            DropColumn("dbo.Tracks", "Chords");
        }
    }
}
