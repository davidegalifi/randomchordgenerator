﻿using System.Data.Entity;
using Jamkie.Domain;
using Jamkie.Repositories.Context;
using Jamkie.Repositories.Contracts;
using Jamkie.Repositories.Generic;

namespace Jamkie.Repositories
{
    public class TrackRepository : Repository<Track>, ITrackRepository
    {
        public TrackRepository(DbContext context) : base(context)
        {
        }

        public JamkieDbContext JamkieContext => Context as JamkieDbContext;

        public void Update(Track track)
        {
            var existingTrack = new Track
            {
                Id = track.Id
            };

            Context.Set<Track>().Attach(existingTrack);
            existingTrack.BarsCount = track.BarsCount;
            existingTrack.Chords = track.Chords;
            existingTrack.Repeat = track.Repeat;
            existingTrack.Tempo = track.Tempo;
        }
    }
}
