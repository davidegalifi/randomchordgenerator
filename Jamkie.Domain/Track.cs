﻿using System;
using System.Collections.Generic;

namespace Jamkie.Domain
{
    public class Track
    {
        public int Id { get; set; }
        
        public string UserId { get; set; }
        public SsoUser SsoUser { get; set; }

        public string Name { get; set; }

        public int Tempo { get; set; }

        public string Chords { get; set; }

        public int BarsCount { get; set; }

        public int Repeat { get; set; }
    }
}
