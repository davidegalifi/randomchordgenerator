﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Jamkie.Domain
{
    public class SsoUser : IdentityUser
    {
        public int Level { get; set; }

        public DateTime JoinDate { get; set; }

        public ICollection<Track> Tracks { get; set; }
    }
}